﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace BarclaysJsonVat
{
    class Program
    {
        static void Main(string[] args)
        {
            var task = Task.Run(async () => await new VatFetcher().GetAsync());

            task.Wait();

            var result = task.Result;

            if (result?.rates != null)
            {
                try
                {
                    var vatOrdered = result.rates
                        .Select(rate => new
                        {
                            country = rate.country_code,
                            period = rate.periods.OrderByDescending(x => x.effective_from).FirstOrDefault(period =>
                                period.EffectiveFrom == DateTime.MinValue || period.EffectiveFrom <= DateTime.Now.Date)
                        }).OrderBy(x => x.period.rates.standard).ToArray();

                    var threeLowest = vatOrdered.Take(3);
                    var threeHighest = vatOrdered.Reverse().Take(3);

                    Console.WriteLine($"Lowest rates:");
                    foreach (var rate in threeLowest)
                        Console.WriteLine($"{rate.country}\t{rate.period.rates.standard}");

                    Console.WriteLine($"Highest rates:");
                    foreach (var rate in threeHighest)
                        Console.WriteLine($"{rate.country}\t{rate.period.rates.standard}");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);                    
                }
            }
            else
            {
                Console.WriteLine("Unable to fetch data from http://jsonvat.com/");
            }

            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
