﻿using System.Collections.Generic;

namespace BarclaysJsonVat
{
    public class Rate
    {
        public string name { get; set; }
        public string code { get; set; }
        public string country_code { get; set; }
        public List<Period> periods { get; set; }
    }
}