﻿using System;
using System.Globalization;

namespace BarclaysJsonVat
{
    public class Period
    {
        public string effective_from { get; set; }
        public Rates rates { get; set; }

        public DateTime EffectiveFrom
        {
            get
            {
                if (DateTime.TryParseExact(effective_from, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out var result))
                    return result;

                return DateTime.MinValue;                
            }
        }
    }
}