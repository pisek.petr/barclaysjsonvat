﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace BarclaysJsonVat
{
    class VatFetcher
    {
        public async Task<JsonVatRoot> GetAsync()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri("http://jsonvat.com/");
                    var response = await client.GetAsync("");
                    response.EnsureSuccessStatusCode();
                    var stringResult = await response.Content.ReadAsStringAsync();
                    var ret = JsonConvert.DeserializeObject<JsonVatRoot>(stringResult);
                    return ret;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }

            return null;
        }
    }
}