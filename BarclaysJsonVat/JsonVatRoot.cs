﻿using System.Collections.Generic;

namespace BarclaysJsonVat
{
    public class JsonVatRoot
    {
        public string details { get; set; }
        public object version { get; set; }
        public List<Rate> rates { get; set; }
    }
}